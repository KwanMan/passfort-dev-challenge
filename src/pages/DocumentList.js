import React from 'react'
import { connect } from 'react-refetch'
import { API } from '../config'
import Loading from '../components/Loading'

const fetchConfig = props => ({
  fetch: `${API}/pages`
})

function DocumentList ({ fetch }) {
  if (fetch.pending) return <Loading />
  if (fetch.rejected) {
    return (
      <div className='error'>Failed fetching documents: {fetch.reason}</div>
    )
  }
  if (fetch.fulfilled) {
    const { titles } = fetch.value
    return (
      <div>
        <h1>Documents</h1>
        <ul>
          {titles.map(title => (
            <li key={title}>
              <a href={`/documents/${title}`}>{title}</a>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export { DocumentList as _DocumentList }
export default connect(fetchConfig)(DocumentList)
