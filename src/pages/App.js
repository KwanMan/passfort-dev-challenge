import React from 'react'
import { css } from 'emotion'

const className = css`
  max-width: 960px;
  margin: 16px auto;
  padding: 0 16px;
`

export default function App ({ children }) {
  return (
    <div className={className}>
      {children}
    </div>
  )
}
