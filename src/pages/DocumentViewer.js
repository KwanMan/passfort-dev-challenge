import React from 'react'
import ReactMarkdown from 'react-markdown'
import { connect, PromiseState } from 'react-refetch'
import { API } from '../config'
import Loading from '../components/Loading'
import RevisionSelector from '../components/RevisionSelector'

const refetchConfig = ({ params }) => {
  const { documentId } = params
  const timestamp = params.timestamp !== undefined ? params.timestamp : 'latest'
  return {
    pageFetch: `${API}/page/${documentId}/${timestamp}`,
    revisionsFetch: `${API}/page/${documentId}`
  }
}
class DocumentViewer extends React.Component {
  render () {
    const { pageFetch, revisionsFetch } = this.props
    const fetch = PromiseState.all([pageFetch, revisionsFetch])
    if (fetch.pending) return <Loading />
    if (fetch.rejected) {
      return (
        <div className='error'>
          There was an error loading the document: {fetch.reason}
        </div>
      )
    }
    return (
      <div>
        {this.renderDocument(pageFetch.value)}
        <hr />
        {this.renderRevisionSelector(revisionsFetch.value)}
      </div>
    )
  }

  handleChangeRevision (timestamp) {
    const { router, params } = this.props
    router.push(`/${params.documentId}/${timestamp}`)
  }

  renderRevisionSelector ({ revisions }) {
    const { params } = this.props
    return (
      <RevisionSelector
        revisions={revisions}
        selectedTimestamp={params.timestamp}
        onChangeRevision={t => this.handleChangeRevision(t)}
      />
    )
  }

  renderDocument ({ data }) {
    return <ReactMarkdown source={data} />
  }
}

export { DocumentViewer as _DocumentViewer }
export default connect(refetchConfig)(DocumentViewer)
