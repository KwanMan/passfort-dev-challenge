import { shallow } from 'enzyme'
import React from 'react'
import { _DocumentList } from '../DocumentList'
import Loading from '../../components/Loading'
import * as mock from './mock'

describe('when loading', () => {
  it('shows the loading icon', () => {
    const rendered = shallow(<_DocumentList fetch={mock.pending()} />)
    expect(rendered.find(Loading)).toHaveLength(1)
  })
})

describe('when failed', () => {
  it('shows failed reason', () => {
    const REASON = 'forthelulz'
    const rendered = shallow(<_DocumentList fetch={mock.rejected(REASON)} />)
    const errorComponent = rendered.find('.error')
    expect(errorComponent).toHaveLength(1)
    expect(errorComponent.text()).toContain(REASON)
  })
})

describe('when loaded', () => {
  it('renders all the available documents', () => {
    const titles = ['foo', 'bar', 'doo', 'dar']
    const fetch = mock.fulfilled({ titles })
    const rendered = shallow(<_DocumentList fetch={fetch} />)
    const renderedItems = rendered.find('li').map(li => li.text())
    expect(renderedItems).toEqual(titles)
  })
})
