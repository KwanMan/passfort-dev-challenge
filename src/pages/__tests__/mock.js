export function pending () {
  return { pending: true }
}

export function fulfilled (value) {
  return { fulfilled: true, value }
}

export function rejected (reason) {
  return { rejected: true, reason }
}

export function router () {
  const router = {
    push: jest.fn(),
    __reset: () => {
      router.push.mockClear()
    }
  }
  return router
}
