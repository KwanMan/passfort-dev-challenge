import { shallow } from 'enzyme'
import React from 'react'
import ReactMarkdown from 'react-markdown'
import { _DocumentViewer } from '../DocumentViewer'
import Loading from '../../components/Loading'
import RevisionSelector from '../../components/RevisionSelector'
import * as mock from './mock'

const CONTENT = `# h1 title\n## h2 subtitle\nthen some content`
const REVISIONS = [Date.now() - 1000, Date.now() - 2000]
const DOCUMENT_ID = 'DOCUMENT_ID'
const TIMESTAMP = Date.now()

describe('when loading page', () => {
  it('shows the loading icon', () => {
    const rendered = shallow(
      <_DocumentViewer
        pageFetch={mock.pending()}
        revisionsFetch={mock.fulfilled()}
      />
    )
    expect(rendered.find(Loading)).toHaveLength(1)
  })
})

describe('when loading revisions', () => {
  it('shows the loading icon', () => {
    const rendered = shallow(
      <_DocumentViewer
        pageFetch={mock.fulfilled()}
        revisionsFetch={mock.pending()}
      />
    )
    expect(rendered.find(Loading)).toHaveLength(1)
  })
})

describe('when loading page failed', () => {
  it('shows failed reason', () => {
    const REASON = 'forthelulz'
    const rendered = shallow(
      <_DocumentViewer
        pageFetch={mock.rejected(REASON)}
        revisionsFetch={mock.fulfilled()}
      />
    )
    const errorComponent = rendered.find('.error')
    expect(errorComponent).toHaveLength(1)
    expect(errorComponent.text()).toContain(REASON)
  })
})

describe('when loading revisions failed', () => {
  it('shows failed reason', () => {
    const REASON = 'forthelulz'
    const rendered = shallow(
      <_DocumentViewer
        revisionsFetch={mock.rejected(REASON)}
        pageFetch={mock.fulfilled()}
      />
    )
    const errorComponent = rendered.find('.error')
    expect(errorComponent).toHaveLength(1)
    expect(errorComponent.text()).toContain(REASON)
  })
})

describe('when loaded', () => {
  let rendered, router
  beforeEach(() => {
    router = mock.router()
    rendered = shallow(
      <_DocumentViewer
        router={router}
        params={{
          documentId: DOCUMENT_ID,
          timestamp: TIMESTAMP
        }}
        pageFetch={mock.fulfilled({ data: CONTENT })}
        revisionsFetch={mock.fulfilled({ revisions: REVISIONS })}
      />
    )
  })
  it('renders the document', () => {
    expect(rendered.find(ReactMarkdown)).toHaveLength(1)
    expect(rendered.find(ReactMarkdown).props().source).toEqual(CONTENT)
  })
  it('renders the revision selector', () => {
    const revisionSelector = rendered.find(RevisionSelector)
    expect(revisionSelector).toHaveLength(1)
    expect(revisionSelector.props()).toEqual({
      selectedTimestamp: TIMESTAMP,
      revisions: REVISIONS,
      onChangeRevision: expect.any(Function)
    })
  })
  it('revision selector changes route', () => {
    const revisionSelector = rendered.find(RevisionSelector)
    expect(revisionSelector).toHaveLength(1)
    const newRevision = 1234
    revisionSelector.props().onChangeRevision(newRevision)
    expect(router.push).toHaveBeenCalledWith(`/${DOCUMENT_ID}/${newRevision}`)
  })
})
