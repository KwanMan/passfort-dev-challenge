import React from 'react'

function Home () {
  return (
    <div>
      <h1>Passfort Wiki</h1>
      <p>See available documents <a href='/documents'>here</a></p>
    </div>
  )
}

export default Home
