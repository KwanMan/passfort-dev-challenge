import React from 'react'
import ReactDOM from 'react-dom'
import createRouter from 'space-router'

import theme from './theme'
import App from './pages/App'
import Home from './pages/Home'
import DocumentList from './pages/DocumentList'
import DocumentViewer from './pages/DocumentViewer'

theme.injectStyles()

// prettier-ignore
const router = createRouter([
  ['', App, [
    ['/', Home],
    ['/documents', DocumentList],
    ['/documents/:documentId', DocumentViewer],
    ['/documents/:documentId/:timestamp', DocumentViewer],
    ['*', props => <div>404</div>]
  ]]
])

function render (route, components) {
  const app = components.reduceRight(
    (children, Component) => (
      <Component params={route.params} router={router}>{children}</Component>
    ),
    null
  )
  ReactDOM.render(app, document.getElementById('root'))
}

router.start(render)
