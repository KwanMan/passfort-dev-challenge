import React from 'react'
import { shallow } from 'enzyme'
import ReactSelect from 'react-select'
import RevisionSelector from '../RevisionSelector'

const revisions = [2, 4, 6, 8]
it('finds correct revision with timestamp', () => {
  const revisionSelector = shallow(
    <RevisionSelector revisions={revisions} selectedTimestamp={5} />
  )
  const reactSelect = revisionSelector.find(ReactSelect)
  expect(reactSelect.props().value.value).toEqual(4)
})

it('finds correct revision with no timestamp', () => {
  const revisionSelector = shallow(<RevisionSelector revisions={revisions} />)
  const reactSelect = revisionSelector.find(ReactSelect)
  expect(reactSelect.props().value.value).toEqual(8)
})
