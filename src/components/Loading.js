import React from 'react'

const STROKE_WIDTH = 3
const SIZE = 24

export default class Loading extends React.Component {
  constructor () {
    super()
    this.state = { progress: 0 }
  }
  componentDidMount () {
    this.interval = setInterval(() => {
      this.setState(state => ({ progress: (state.progress + 6) % 720 }))
    }, 30)
  }
  componentWillUnmount () {
    clearInterval(this.interval)
  }
  render () {
    const { progress } = this.state
    const normalised = progress % 720
    const value = (normalised >= 360 ? normalised - 720 : normalised) / 360
    const R = 16 - STROKE_WIDTH
    const C = 2 * Math.PI * R
    return (
      <svg viewBox='0 0 32 32' width={SIZE || 128} height={SIZE || 128}>
        <circle
          cx={16}
          cy={16}
          r={R}
          fill='none'
          stroke='blue'
          strokeWidth={STROKE_WIDTH}
          opacity='0.125'
        />
        <circle
          cx={16}
          cy={16}
          r={R}
          fill='none'
          stroke='blue'
          strokeWidth={STROKE_WIDTH}
          strokeDasharray={C}
          strokeDashoffset={C - value * C}
          transform='rotate(-90 16 16)'
        />
      </svg>
    )
  }
}

Loading.propTypes = {}
