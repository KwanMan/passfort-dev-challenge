import React from 'react'
import ReactSelect from 'react-select'
import format from 'date-fns/format'
import _ from 'lodash'

export default function RevisionSelector (props) {
  const { revisions, selectedTimestamp, onChangeRevision } = props
  const options = revisions.map((timestamp, i) => {
    let label = format(new Date(timestamp * 1000), 'ddd DD MMMM YYYY HH:mm')
    if (i === revisions.length - 1) {
      label += ` (Latest)`
    }
    return {
      value: timestamp,
      label
    }
  })

  const currentOption = _.isUndefined(selectedTimestamp)
    ? _.last(options)
    : _.findLast(options, o => Number(selectedTimestamp) >= o.value)

  return (
    <div>
      <div>Select a revision:</div>
      <ReactSelect
        options={options}
        value={currentOption}
        onChange={e => onChangeRevision(e.value)}
      />
    </div>
  )
}
