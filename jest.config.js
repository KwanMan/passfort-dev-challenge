module.exports = {
  testMatch: ['**/__tests__/*-test.js'],
  setupTestFrameworkScriptFile: '<rootDir>/src/setupTests.js',
  testURL: 'http://localhost/'
}
